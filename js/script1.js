
let menu = document.querySelector('#menu-btn');
let header = document.querySelector('.header');

menu.onclick = function(){
  menu.classList.toggle('fa-times');
  header.classList.toggle('active');
}
window.onscroll = function(){
  menu.classList.remove('fa-times');
  header.classList.remove('active');
}

$(function(){
  var top = $('#pagetop');
  top.hide();
  $(window).scroll(function(){
    if( $(window).scrollTop() > 300){
      top.fadeIn();
    }else{
      top.fadeOut();
    }
  });

  top.click(function(){
    $('body,html').animate({
      scrollTop: 0}, 50 );
      return false;
    });
  });


$(function(){
  $('.home-slider').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      items:1,
      autoplay:true
  })

});
